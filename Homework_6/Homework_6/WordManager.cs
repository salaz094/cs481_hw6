﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homework_6
{
    class WordManager
    {
		IRestService restService;

		public WordManager(IRestService service)
		{
			restService = service;
		}

		public Task<List<Word>> GetTasksAsync()
		{
			return restService.RefreshDataAsync();
		}

		public Task SaveTaskAsync(Word item, bool isNewWord = false)
		{
			return restService.SaveWordAsync(item, isNewWord);
		}

		public Task DeleteTaskAsync(Word item)
		{
			return restService.DeleteWordAsync(item.Definition);
		}
	}
}
