﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homework_6
{
   
		public interface IRestService
		{
		Task<List<Word>> RefreshDataAsync();

		Task SaveWordAsync(Word item, bool isNewWord);

		Task DeleteWordAsync(string id);
		}
	
}
