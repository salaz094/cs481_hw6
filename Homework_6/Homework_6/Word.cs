﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework_6
{ 
    class Word
    {
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Example { get; set; }
    }
}
