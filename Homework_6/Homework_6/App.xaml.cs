﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;



namespace Homework_6
{
    public partial class App : Application
    {
        public WordManager WrdManager { get; set; }
        public App()
        {
            InitializeComponent();
            WrdManager = new WordManager(new RestService()) ;
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
